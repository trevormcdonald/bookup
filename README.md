# bookup
Input a picture of the ISBN code of a book, get back the author and title.

Run as:
`python bookup.py --image path/to/image.png`

Things to install (`apt-get`, `brew`, etc):
* zbar (`apt-get install libzbar0`, `brew install zbar`)

Things to `pip install`:

* pyzbar
* opencv-python

This also uses argparse and requests.

pyzbar is a wrapper for zbar by the Natural History Museum.
openlibrary is an api provided by The Internet Archive.
